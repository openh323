/* H460_OID1.cxx
 *
 * Copyright (c) 2004 ISVO (Asia) Pte Ltd. All Rights Reserved.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is derived from and used in conjunction with the 
 * OpenH323 Project (www.openh323.org/)
 *
 * The Initial Developer of the Original Code is ISVO (Asia) Pte Ltd.
 *
 *
 * Contributor(s): ______________________________________.
 *
 * $Log$
 * Revision 1.7  2006/07/21 10:32:47  shorne
 * Added ARQ message support for differential call routing and charging
 *
 * Revision 1.6  2006/07/17 09:16:18  shorne
 * More fine tuning
 *
 * Revision 1.5  2006/07/11 04:40:25  shorne
 * Fix to stop IM Sessions unexpectedly dropping out
 *
 * Revision 1.4  2006/07/09 12:49:31  shorne
 * Bring up to date with first commercial release
 *
 * Revision 1.3  2006/06/15 15:43:37  shorne
 * Restructured the way H460 OID is handled
 *
 * Revision 1.2  2006/05/16 18:49:58  shorne
 * Added more ReleaseComplete notifications
 *
 * Revision 1.1  2006/05/16 16:03:38  shorne
 * Initial commit
 *
 *
 */

#include <ptlib.h>

#include "h460_oid1.h"
#include <h323pdu.h>

#include "testapp/main.h"


static const char * baseOID  = "1.3.6.1.4.1.17090.0.1";      // Advertised Feature
static const char * typeOID  = "1";                          // Type 1-IM session 
static const char * encOID   = "3";                          // Support Encryption
static const char * OpenOID  = "4";                          // Message Session open/close
static const char * MsgOID   = "5";                          // Message contents
static const char * WriteOID  = "6";                         // Message write event
static const char * InviteOID  = "7";                        // Invite Message


static const char * RegOID     = "10";
static const char * RegIDOID   = "10.1";
static const char * RegPwdOID  = "10.2";

#ifdef _MSC_VER
#pragma warning(disable : 4239)
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Must Declare for Factory Loader.
H460_FEATURE(OID1);

H460_FeatureOID1::H460_FeatureOID1()
: H460_FeatureOID(baseOID)
{
 PTRACE(4,"OID1\tInstance Created");

 remoteSupport = FALSE;
 remoteEnc = FALSE;
 callToken = PString();
 sessionOpen = FALSE;

 EP = NULL;
 CON = NULL;
 FeatureCategory = FeatureSupported;

}

H460_FeatureOID1::~H460_FeatureOID1()
{
}

void H460_FeatureOID1::AttachEndPoint(H323EndPoint * _ep)
{
PTRACE(4,"OID1\tEndPoint Attached");
   EP = (MyH323EndPoint *)_ep; 
}

void H460_FeatureOID1::AttachConnection(H323Connection * _con)
{
   CON = (MyH323Connection *)_con;
   callToken = _con->GetCallToken();
}

BOOL H460_FeatureOID1::OnSendSetup_UUIE(H225_FeatureDescriptor & pdu) 
{

  // Set Call Token
  callToken = CON->GetCallToken();

  // Build Message
  H460_FeatureOID & feat = H460_FeatureOID(baseOID); 

  // Is a IM session call
  if (CON->IMCall) {
      sessionOpen = !CON->IMsession;                // set flag to open session 
      feat.Add(typeOID,H460_FeatureContent(1,8));   // 1 specify Instant Message Call
      CON->DisableH245inSETUP();                    // Turn off H245 in Setup
  }

//  feat.Add(encOID,H460_FeatureContent(true));   // false Support Encryption
  
  // Is Gateway Registration Call
  if ((!CON->IMReg) && (CON->IMRegID.GetLength() > 0)) {
       H460_FeatureTable tab;
	   feat.SetCurrentTable(tab);
          feat.Add(RegIDOID,H460_FeatureContent(CON->IMRegID));
          feat.Add(RegPwdOID,H460_FeatureContent(CON->IMRegPwd));
       feat.SetDefaultTable();
       feat.Add(RegOID,H460_FeatureContent(tab));
  } 


  // Attach PDU
  pdu = feat;

  return TRUE;
}

void H460_FeatureOID1::OnReceiveSetup_UUIE(const H225_FeatureDescriptor & pdu) 
{

   callToken = CON->GetCallToken();

   H460_FeatureOID & feat = (H460_FeatureOID &)pdu;

   if (feat.Contains(typeOID)) {  // This is a Non Call Service
	      CON->DisableH245inSETUP();    // Turn off H245 Tunnelling in Setup

     unsigned calltype = feat.Value(typeOID);
     if (calltype == 1)   // IM Call
         CON->IMCall = TRUE;
	  
   }

//  Remote supports Encryption
//  if (feat.Contains(OpalOID(encOID))) 
//		  remoteEnc = feat.Value(OpalOID(encOID));   

   if (feat.Contains(RegOID))   // external gateway management
   {

   }
   CON->IMsupport = TRUE;
   EP->IMSupport(callToken);
   remoteSupport = TRUE; 

}

BOOL H460_FeatureOID1::OnSendCallProceeding_UUIE(H225_FeatureDescriptor & pdu) 
{ 
    if (remoteSupport) {
// Build Message
      H460_FeatureOID & feat = H460_FeatureOID(baseOID); 

// Signal to say ready for message
      if (CON->IMCall)
          feat.Add(typeOID,H460_FeatureContent(1,8));   // Notify ready for message

// Responsed Support Encryption
//    if (remoteEnc)
//       feat.Add(encOID,H460_FeatureContent(true));

// Attach PDU
       pdu = feat;
    }

	return remoteSupport; 
}

void H460_FeatureOID1::OnReceiveCallProceeding_UUIE(const H225_FeatureDescriptor & pdu) 
{
 
   remoteSupport = TRUE;
   EP->IMSupport(callToken);
   CON->IMsupport = TRUE;

   H460_FeatureOID & feat = (H460_FeatureOID &)pdu;

//  Remote Supports Encryption
//  if (feat.Contains(encOID))
//		 remoteEnc = feat.Value(encOID);

//   if Remote Ready for Non-Call 
    if (feat.Contains(typeOID)) { 
       unsigned calltype = feat.Value(typeOID);

//    if IM session send facility msg
       if (calltype == 1) {  
         H323SignalPDU facilityPDU;
         facilityPDU.BuildFacility(*CON, FALSE);
         CON->WriteSignalPDU(facilityPDU);
       }
    } 
}


// Send Message
BOOL H460_FeatureOID1::OnSendFacility_UUIE(H225_FeatureDescriptor & pdu) 
{ 

    if (remoteSupport) {
	// Build Message
      H460_FeatureOID & feat = H460_FeatureOID(baseOID); 
      BOOL contents = FALSE;

     // Open and Closing Session
     if ((sessionOpen != CON->IMsession)) {
          sessionOpen = CON->IMsession;
          feat.Add(OpenOID,H460_FeatureContent(sessionOpen));

       if (sessionOpen)
          EP->IMSessionOpen(callToken);
       else if (!CON->IMCall)
          EP->IMSessionClosed(callToken); 

       contents = TRUE;
     }

	// If Message send as Unicode String
      if (CON->IMmsg.GetLength() > 0) {
         PASN_BMPString str; 
         str.SetValue(CON->IMmsg);
         feat.Add(MsgOID,H460_FeatureContent(str));
         CON->IMmsg = PString();
		 contents = TRUE;
      }

      if (contents) {
         pdu = feat;

        if (CON->IMCall && !sessionOpen) {
            if (feat.Contains(MsgOID))
 			  EP->IMSent(callToken,TRUE);
			else 
			  EP->IMSessionClosed(callToken);
		 }
         return TRUE;
      }

    }

    return FALSE; 
};

// Receive Message
void H460_FeatureOID1::OnReceiveFacility_UUIE(const H225_FeatureDescriptor & pdu) 
{
   H460_FeatureOID & feat = (H460_FeatureOID &)pdu;
   BOOL open = FALSE;

   if (feat.Contains(OpenOID)) {
	   open = feat.Value(OpenOID);
	   if (open) {
             EP->IMSessionOpen(callToken);
             CON->IMsession = TRUE;
             CON->SetCallAnswered();   // Set Flag to specify call is answered 
	   } else {
             if (CON->IMsession)
                 EP->IMSessionClosed(callToken);

             CON->IMsession = FALSE;
	   }
	   sessionOpen = open;
   }

   if (feat.Contains(MsgOID)) {
	PASN_BMPString & str = feat.Value(MsgOID);
       EP->IMReceived(callToken,str.GetValue(),CON->IMsession);
   }

   if (!CON->IMCall)   // Message in an existing connection
	   return;

   if (open) {
       H323SignalPDU connectPDU;
       connectPDU.BuildConnect(*CON);
       CON->WriteSignalPDU(connectPDU); // Send H323 Connect PDU
   } else if (!CON->IMsession) {
       CON->ClearCall();    // Send Release Complete
   }

};

// You end connection
BOOL H460_FeatureOID1::OnSendReleaseComplete_UUIE(H225_FeatureDescriptor & pdu) 
{ 
	if (sessionOpen) {
		if (CON->IMsession)
           EP->IMSessionClosed(callToken);
	   sessionOpen = FALSE;

	// Build Message
           H460_FeatureOID & feat = H460_FeatureOID(baseOID); 
           feat.Add(OpenOID,H460_FeatureContent(sessionOpen));
           pdu = feat;
           return TRUE;
	}

   return FALSE; 
};

// Other person ends connection
void H460_FeatureOID1::OnReceiveReleaseComplete_UUIE(const H225_FeatureDescriptor & pdu) 
{
   H460_FeatureOID & feat = (H460_FeatureOID &)pdu;

    if (sessionOpen && feat.Contains(OpenOID)) {
      BOOL open = feat.Value(OpenOID);
	if (!open) {
          sessionOpen = FALSE;
          EP->IMSessionClosed(callToken);
        }
    }
};  

BOOL H460_FeatureOID1::OnSendAdmissionRequest(H225_FeatureDescriptor & pdu) 
{ 
   if (CON->IMCall)   // Message in an IM Call
   {
       H460_FeatureOID & feat = H460_FeatureOID(baseOID); 
       feat.Add(typeOID,H460_FeatureContent(1,8));   // 1 specify Instant Message Call
       pdu = feat;
	   return TRUE;
   }

	return FALSE; 	
}

#ifdef _MSC_VER
#pragma warning(default : 4239)
#endif